* ix-emacs

Home of my Emacs configuration.

** Usage

Grab a copy of this repository and put it in your ~~/.emacs.d~.

*** Home Manager

Using [[https://nix-community.github.io/home-manager/][Home Manager]] you can automate building ix-emacs and linking your
~~/.emacs.d~ to the result.

The example below shows how to use the package list included in this repository
(~nix/emacs-packages.nix~).

The ~EMACS_FLUID_DIRECTORY~ is set to a path outside of the Nix store, to allow
installing additional packages (since ~~/.emacs.d~ is read-only being a link to
the Nix store). By setting it to ~~/.cache/emacs~ ix-emacs will setup emacs to
write any fluid (user) data there.

#+BEGIN_EXAMPLE
{ config, pkgs, lib, ... }:

let
  inherit (pkgs) stdenv fetchFromGitLab;
  inherit (config) home programs;

  ix-emacs-src = fetchFromGitLab {
    owner = "bjpbakker";
    repo = "emacs.d";
    rev = ..;
    sha256 = ..;
  };

  ix-emacs-packages = import "${ix-emacs-src}/nix/emacs-packages.nix";

  ix-emacs-d = stdenv.mkDerivation {
    pname = "ix-emacs.d";
    version = ix-emacs-src.rev;
    src = ix-emacs-src;

    patchPhase = ''
      substituteInPlace init.el \
          --replace '(getenv "EMACS_FLUID_DIRECTORY")' '"${home.homeDirectory}/.cache/emacs"'
    '';

    installPhase = ''
      prefix=$out make install
    '';
  };

in
{
  programs.emacs = {
    enable = true;
    extraPackages = ix-emacs-packages;
  };

  home.file.".emacs.d" = {
    source = ix-emacs-d;
  };
}
#+END_EXAMPLE

** License

Copyright (C) Bart Bakker

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <https://www.gnu.org/licenses/>.
