;;; -*- lexical-binding: t -*-
;;; ix-yaml.el --- Make YAML slightly less embarrassing.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package yaml-mode
  :preface
  (defun ix/toggle-fold ()
    "Toggle fold all lines larger than indentation on current line"
    (interactive)
    (let ((col 1))
      (save-excursion
        (back-to-indentation)
        (setq col (+ 1 (current-column)))
        (set-selective-display
         (if selective-display nil (or col 1))))))

  :bind (:map yaml-mode-map ("M-C-i" . ix/toggle-fold))
  :config
  (with-eval-after-load 'flycheck
    (add-hook 'yaml-mode-hook #'flycheck-mode)))

(use-package highlight-indentation
  :hook (yaml-mode . highlight-indentation-mode))

(use-package smart-shift
  :pin "melpa-unstable"
  :hook (yaml-mode . smart-shift-mode))

(provide 'ix-yaml)
