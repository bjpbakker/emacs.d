;;; -*- lexical-binding: t -*-
;;; ix-defaults.el --- Provide modern defaults.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

;; Start with a nice clean slate.
(setq inhibit-startup-screen t
      inhibit-splash-screen t
      inhibit-startup-echo-area-message "ix-emacs"
      initial-buffer-choice t
      initial-scratch-message nil)

(blink-cursor-mode 0)

;; Turn of beeping
(setq visible-bell t)

(setq use-dialog-box nil)
(if (boundp 'use-short-answers)
    (setq use-short-answers t)
  (defalias 'yes-or-no-p 'y-or-n-p))

;; Don't suspend with C-z on window systems, but do keep it in terminal.
(when (display-graphic-p)
  (global-unset-key (kbd "C-z")))

;; Use UTF-8 as the default coding system.
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq-default buffer-file-coding-system 'utf-8)
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;; Improve default scrolling.
(setq auto-window-vscroll nil
      scroll-margin 5
      scroll-conservatively 10000
      scroll-preserve-screen-position nil
      mouse-wheel-progressive-speed nil)

;; We don't need to learn about C-h C-a on every boot.
(defun display-startup-echo-area-message ()
  (message "Happy hacking!"))

;; Automatically update unmodified buffers whose contents change elsewhere.
(require 'autorevert)
(global-auto-revert-mode t)
(setq global-auto-revert-non-file-buffers t)

(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets
      uniquify-strip-common-suffix nil
      uniquify-after-kill-buffer-p t
      uniquify-ignore-buffers-re "^\\*")

(require 'compile)
(setq compilation-ask-about-save nil
      compilation-scroll-output 'first-error)

;; Handle ANSI color output in compilation buffers
;; From: https://gist.github.com/jwiegley/8ae7145ba5ce64250a05
(defun compilation-ansi-color-process-output ()
  (ansi-color-process-output nil)
  (set (make-local-variable 'comint-last-output-start)
       (point-marker)))
(add-hook 'compilation-filter-hook #'compilation-ansi-color-process-output)

;; Increase the buffer size emacs can read from a process. The default 4k is
;; rather low.
(setq read-process-output-max 16384)

;; Make C-g quit minibuffers first
(defun ix/quit ()
  "Quit minibuffers or keyboard-quit"
  (interactive)
  (cond ((region-active-p) (keyboard-quit))
        ((derived-mode-p 'completion-list-mode) (delete-completion-window))
        ((> (minibuffer-depth) 0) (abort-recursive-edit))
        (t (keyboard-quit))))

(bind-key "C-g" #'ix/quit)

(provide 'ix-defaults)
