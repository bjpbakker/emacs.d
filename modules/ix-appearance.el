;;; -*- lexical-binding: t -*-
;;; ix-appearance.el --- Aesthetics.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'colour)
(require 'use-package)

(defun ix/font-installed-p (font)
  (not (null (x-list-fonts font))))

(menu-bar-mode -1)
(when (display-graphic-p)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (tooltip-mode -1)

  (let ((font
         (cond ((ix/font-installed-p "Iosevka Extended") "Iosevka Extended 10")
               ((ix/font-installed-p "FiraCode") "FiraCode 10")
               ((ix/font-installed-p "Hack") "Hack 10")
               (t "Monospace 10"))))
    (setq default-frame-alist `((width . 160)
                                (height . 50)
                                (left . 50)
                                (top . 50)
                                (font . ,font))
          initial-frame-alist default-frame-alist))

  (set-frame-parameter nil 'undecorated t))

  (defun ix-theme/light ()
    "Apply IX light theme"
    (interactive)
    (use-package material-theme
      :config
      (load-theme 'material-light t)

      (eval-after-load 'volatile-highlights
        '(set-face-background 'vhl/default-face "#d0d9dd"))))

(defun ix-theme/dark ()
  "Apply IX dark theme"
  (interactive)
  (use-package ir-black-theme
    :config
    (load-theme #'ir-black t)

    (set-face-attribute 'vertical-border nil :foreground "gray20")

    (with-eval-after-load 'hl-line
      (set-face-attribute 'hl-line nil :underline nil))

    (with-eval-after-load 'smart-mode-line
      (set-face-attribute 'mode-line nil :box nil)
      (set-face-attribute 'mode-line-inactive nil :background "#101010"))))

(setq column-number-mode t)

(use-package diminish
  :config
  (eval-after-load 'abbrev '(diminish 'abbrev-mode))
  (eval-after-load 'eldoc '(diminish 'eldoc-mode)))

(use-package smart-mode-line
  :config
  (setq sml/theme 'respectful
        sml/shorten-directory t
        sml/name-width 40
        sml/mode-width 'full)
  (let ((sml/no-confirm-load-theme t))
    (sml/setup)))

;; Start with default dark theme
(ix-theme/dark);

(provide 'ix-appearance)
