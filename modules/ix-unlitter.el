;;; -*- lexical-binding: t -*-
;;; ix-unlitter.el --- Unlitter .emacs.d

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'emacs-paths)

(defmacro unlitter (package args)
  (declare (indent 1))
  `(eval-after-load ',package
     (,@(cons `setq
              (mapcan
               (lambda (def)
                 (let ((variable (car def))
                       (value (cadr def)))
                   `(,variable (unlitter-path ,value))))
               (if (listp (car args)) args (list args)))))))

(defun unlitter-path (file)
  (let* ((basedir (emacs-fluid-path "unlitter"))
         (path (expand-file-name file basedir)))
    (make-directory (file-name-directory path) t)
    path))

(setq auto-save-list-file-prefix (unlitter-path "auto-save/")
      backup-directory-alist (list (cons "." (unlitter-path "backup/")))
      bookmark-default-file (unlitter-path "bookmark-default.el")
      tramp-auto-save-directory (unlitter-path "tramp/auto-save/")
      tramp-persistency-file-name (unlitter-path "tramp/tramp")
      url-cookie-file (unlitter-path "url/cookies"))

(unlitter recentf
  (recentf-save-file "recentf"))

 (unlitter projectile
   ((projectile-cache-file "projectile/cache.el")
    (projectile-known-projects-file "projectile/bookmarks.el")))

(unlitter multiple-cursors
  (mc/list-file "mc-lists.el"))

(unlitter transient
  ((transient-history-file "transient/history.el")
   (transient-levels-file "transient/levels.el")
   (transient-values-file "transient/values.el")))

(when (native-comp-available-p)
  (startup-redirect-eln-cache (unlitter-path "eln/cache")))

(unlitter async-bytecomp
  (async-byte-compile-log-file "async-bytecomp.log"))

(unlitter dap-mode
  (dap-breakpoints-file "dap/breakpoints"))

(provide 'ix-unlitter)
