;;; -*- lexical-binding: t -*-
;;; ix-flycheck.el --- Flycheck syntax checker and linter.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package flycheck
  :hook (prog-mode . flycheck-mode)
  :commands (flycheck-mode
             flycheck-next-error
             flycheck-previous-error)

  :bind (:map flycheck-mode-map
              ("M-n" . flycheck-next-error)
              ("M-p" . flycheck-previous-error))

  :config
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

(provide 'ix-flycheck)
