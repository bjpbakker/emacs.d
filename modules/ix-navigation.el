;;; -*- lexical-binding: t -*-
;;; ix-navigation.el --- General navigation.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(setq scroll-error-top-bottom t)

(use-package ace-window
  :pin "melpa-unstable"
  :config
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)
        aw-scope 'frame)
  :bind* (("C-x o" . ace-window)
          ("C-x C-O" . ace-swap-window)))

(defun ix/smart-beginning-of-line ()
  "Move point to first non-whitespace character (indentation) or beginning-of-line."
  (interactive "^")
  (let ((pos (point)))
    (back-to-indentation)
    (and (= pos (point))
         (beginning-of-line))))
(global-set-key (kbd "C-a") #'ix/smart-beginning-of-line)
(global-set-key (kbd "<home>") #'ix/smart-beginning-of-line)

(global-set-key (kbd "C-c <") #'previous-buffer)
(global-set-key (kbd "C-c >") #'next-buffer)

(use-package deadgrep
  :bind ("M-s g" . deadgrep))

(provide 'ix-navigation)
