;;; -*- lexical-binding: t -*-
;;; ix-elisp.el --- Elisp programming enhancements.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package aggressive-indent
  :diminish
  :hook (emacs-lisp-mode . aggressive-indent-mode))

(use-package highlight-parentheses
  :pin "melpa-unstable"
  :diminish
  :commands highlight-parentheses-mode
  :hook (emacs-lisp-mode . highlight-parentheses-mode))

(use-package highlight-defined
  :pin "melpa-unstable"
  :diminish
  :commands highlight-defined-mode
  :hook (emacs-lisp-mode . highlight-defined-mode))

(use-package eros
  :pin "melpa-unstable"
  :diminish
  :commands eros-mode
  :hook (emacs-lisp-mode . eros-mode))

(provide 'ix-elisp)
