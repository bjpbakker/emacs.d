;;; -*- lexical-binding: t -*-
;;; ix-package.el --- Package configuration.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

;; Initialize all packages in ix-emacs/elpa, and then change the
;; package-user-dir after initializion to the fluid directory.
(setq package-user-dir (emacs-fluid-path "elpa")
      package-quickstart-file (emacs-fluid-path "package-quickstart.el"))

(require 'package)
(setq package-archives
      '(("elpa" . "https://elpa.gnu.org/packages/")
        ("melpa" . "https://stable.melpa.org/packages/")
        ("melpa-unstable" . "https://melpa.org/packages/")))

;; Activate package.el and the installed packages
(package-initialize)

(unless (package-installed-p 'use-package)
  (warn "use-package is not installed, trying to install")

  ;; Fetch the package archives when there's no local copy yet, or
  ;; we cannot use `package-install'.
  (unless package-archive-contents
    (package-refresh-contents))

  (package-install 'use-package))

(require 'use-package)

;; Make use-package install declared packages automatically and pin them to melpa
;; by default.
(setq use-package-always-pin "melpa"
      use-package-always-ensure t)

(provide 'ix-package)
