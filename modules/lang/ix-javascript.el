;;; -*- lexical-binding: t -*-
;;; ix-javascript.el --- Make Javascript programming bearable.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package js
  :mode (("\\.js\\'" . js-mode)
         ("\\.mjs\\'" . js-mode))
  :preface
  (defun prettify-javascript-hook ()
    (let ((javascript-prettify-symbols-alist
           '(("=>"     . ?⇒)
             ("!"      . ?¬)
             ("&&"     . ?∧)
             ("||"     . ?∨)
             ("!="     . ?≠)
             ("==="    . ?≡)
             ("!=="    . ?≢)
             ("<="     . ?≤)
             (">="     . ?≥)

             ("undefined"          . ?⊥))))
      (setq-local prettify-symbols-alist javascript-prettify-symbols-alist))
    (prettify-symbols-mode))
  :config
  (add-hook 'js-mode-hook #'prettify-javascript-hook))

(use-package js2-mode
  :pin "melpa-unstable"
  :when (version< emacs-version "27.0")
  :mode ("\\.js\\'" "\\.mjs\\'"))

(use-package rjsx-mode
  :pin "melpa-unstable"
  :mode (("\\.jsx\\'" . rjsx-mode)))

(use-package npm-mode
  :pin "melpa-unstable"
  :commands (npm-mode npm-mode-npm-run npm-mode-visit-project-file))

(use-package mocha
  :pin "melpa-unstable"
  :commands (mocha-test-project mocha-test-file mocha-test-at-point))

(use-package nodejs-repl
  :commands nodejs-repl)

(provide 'ix-javascript)
