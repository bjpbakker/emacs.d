;;; -*- lexical-binding: t -*-
;;; ix-javascript.el --- Typescript programming enhancements.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package typescript-mode
  :pin "melpa-unstable"
  :mode ("\\.ts\\'"))

(use-package tide
  :after typescript-mode
  :preface
  (defun ix/setup-tide-mode ()
    (interactive)
    (tide-setup)
    (eldoc-mode)
    (tide-hl-identifier-mode))
  :init
  (add-hook 'typescript-mode-hook #'ix/setup-tide-mode))

(use-package deno-fmt
  :pin "melpa-unstable"
  :preface
  (defun ix/deno-project-p ()
    (let ((root (projectile-project-root)))
      (and (not (null root))
           (or (file-exists-p (expand-file-name "deno.jsonc" root))
               (file-exists-p (expand-file-name "deno.json" root))))))

  (defun ix/setup-deno-fmt ()
    (interactive)
    (when (ix/deno-project-p)
      (deno-fmt-mode 1)))

  :init
  (add-hook 'typescript-mode-hook #'ix/setup-deno-fmt))

(use-package flycheck-deno
  :pin "melpa-unstable"
  :after (deno-fmt flycheck)
  :init
  ;; don't use 'flycheck-deno-setup to avoid eagerly loading the package
  (add-to-list 'flycheck-checkers #'deno-lint))

(provide 'ix-typescript)
