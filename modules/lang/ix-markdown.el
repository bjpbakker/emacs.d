;;; -*- lexical-binding: t -*-
;;; ix-markdown.el --- Inferior org-mode.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package markdown-mode
  :mode (("\\.markdown$" . gfm-mode)
         ("\\.md$" . gfm-mode))
  :commands (markdown-mode gfm-mode)
  :config
  (setq markdown-command "pandoc -f markdown"
        markdown-open-command (pcase system-type
                                ('darwin "open")
                                ('gnu/linux "xdg-open"))
        markdown-enable-math t
        markdown-make-gfm-checkboxes-buttons t
        markdown-fontify-whole-heading-line t)
  (add-hook 'markdown-mode-hook 'visual-line-mode))

(provide 'ix-markdown)
