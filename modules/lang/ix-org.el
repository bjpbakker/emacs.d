;;; -*- lexical-binding: t -*-
;;; ix-org.el --- org-mode configuration.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package org
  :ensure nil
  :preface
  (defun ix/org-mode-hook ()
    (visual-line-mode)
    (variable-pitch-mode)

    (let ((tuple '(:background nil :box nil)))
      (apply #'set-face-attribute 'org-document-title nil tuple)
      (dolist (level (number-sequence 1 8))
        (let ((face (intern (format "org-level-%d"))))
          (apply #'set-face-attribute face nil tuple)))))

  :custom
  (org-log-state-notes-into-drawer t)
  (org-ellipsis "…")
  (org-hide-emphasis-markers t)
  (org-hide-leading-stars t)
  (org-fontify-done-headline t)
  (org-fontify-whole-heading-line t)
  (org-fontify-quote-and-verse-blocks t)
  (org-pretty-entities t)
  (org-tags-column 0)
  :config
  (add-hook 'org-mode-hook #'ix/org-mode-hook))

(use-package org-indent
  :ensure nil
  :diminish
  :hook (org-mode . org-indent-mode)
  :custom
  (org-indent-indentation-per-level 2)
  :config
  (set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch)))

(use-package org-superstar
  :hook (org-mode . org-superstar-mode))

(use-package olivetti
  :diminish
  :hook (org-mode . olivetti-mode))

(provide 'ix-org)
