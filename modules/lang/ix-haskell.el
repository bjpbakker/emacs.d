;;; -*- lexical-binding: t -*-
;;; ix-haskell.el --- Haskell programming enhancements.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package haskell-mode
  :diminish
  :mode (("\\.hs\\'" . haskell-mode)
         ("\\.lhs\\'" . haskell-literate-mode)
         ("\\.cabal\\'" . haskell-cabal-mode))
  :bind (:map haskell-mode-map
              ("C-c C-," . haskell-navigate-imports)
              ("C-c C-." . haskell-mode-format-imports)
              ("C-c C-u" . ix/haskell-insert-undefined)
              ("M-s")
              ("M-t"))

  :preface
  (defun ix/haskell-insert-undefined ()
    (interactive)
    (insert "undefined"))

  (defun prettify-haskell-hook ()
    (let ((haskell-prettify-symbols-alist
           '(("::"     . ?∷)
             ("forall" . ?∀)
             ("exists" . ?∃)
             ("->"     . ?→)
             ("<-"     . ?←)
             ("=>"     . ?⇒)
             ("~>"     . ?⇝)
             ("<~"     . ?⇜)
             ("<>"     . ?⨂)
             ("msum"   . ?⨁)
             ("\\"     . ?λ)
             ("not"    . ?¬)
             ("&&"     . ?∧)
             ("||"     . ?∨)
             ("/="     . ?≠)
             ("<="     . ?≤)
             (">="     . ?≥)
             ("<<<"    . ?⋘)
             (">>>"    . ?⋙)

             ("`elem`"             . ?∈)
             ("`notElem`"          . ?∉)
             ("`member`"           . ?∈)
             ("`notMember`"        . ?∉)
             ("`union`"            . ?∪)
             ("`intersection`"     . ?∩)
             ("`isSubsetOf`"       . ?⊆)
             ("`isNotSubsetOf`"    . ?⊄)
             ("`isSubsequenceOf`"  . ?⊆)
             ("`isProperSubsetOf`" . ?⊂)
             ("undefined"          . ?⊥))))
      (setq-local prettify-symbols-alist haskell-prettify-symbols-alist))
    (prettify-symbols-mode))

  :config
  (require 'haskell)
  (require 'haskell-doc)
  (require 'haskell-commands)

  (defun ix/haskell-mode-hook ()
    (haskell-indentation-mode)
    (interactive-haskell-mode)
    (with-eval-after-load 'diminish
      (diminish 'interactive-haskell-mode))
    (prettify-haskell-hook)
    (bug-reference-prog-mode 1))

  (eval-after-load 'align
    '(nconc
      align-rules-list
      (mapcar #'(lambda (x)
                  `(,(car x)
                    (regexp . ,(cdr x))
                    (modes quote (haskell-mode literate-haskell-mode))))
              '((haskell-types       . "\\(\\s-+\\)\\(::\\|∷\\)\\s-+")
                (haskell-assignment  . "\\(\\s-+\\)=\\s-+")
                (haskell-arrows      . "\\(\\s-+\\)\\(->\\|→\\)\\s-+")
                (haskell-left-arrows . "\\(\\s-+\\)\\(<-\\|←\\)\\s-+")))))

  (add-hook 'haskell-mode-hook #'ix/haskell-mode-hook))

(provide 'ix-haskell)
