;;; -*- lexical-binding: t -*-
;;; ix-scala.el --- Scala language configuration.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package scala-mode
  :mode "\\.scala\\'"
  :interpreter
  ("scala" . scala-mode)
  :preface
  (defun prettify-scala-hook ()
    (let ((custom-symbols-alist
           '(("???" . ?⟂))))
      (setq-local
       prettify-symbols-alist
       (append scala-prettify-symbols-alist custom-symbols-alist)))
    (prettify-symbols-mode))
  :custom
  (scala-indent:default-run-on-strategy scala-indent:eager-strategy)
  (scala-indent:align-forms t)
  (scala-indent:align-parameters t)
  :config
  (add-hook 'scala-mode-hook #'prettify-scala-hook)
  (add-hook 'scala-mode-hook #'ix/lsp-join-session))

(use-package sbt-mode
  :mode "\\.sbt\\'")

(use-package lsp-metals
  :after lsp-mode
  :custom
  (lsp-metals-server-args '("-J-Dmetals.allow-multiline-string-formatting=off")))

(provide 'ix-scala)
