;;; -*- lexical-binding: t -*-
;;; ix-ivy.el --- Ivy completion.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package ivy
  :pin "melpa-unstable"
  :demand t
  :diminish
  :bind (("C-c C-r" . ivy-resume)
         :map ivy-minibuffer-map
         ("<RET>" . ivy-alt-done)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-switch-buffer-kill))
  :config
  (setq ivy-dynamic-exhibit-delay-ms 200
        ivy-magic-tilde nil
        ivy-re-builders-alist '((t . ivy--regex-ignore-order))
        ivy-use-virtual-buffers t
        ivy-wrap t)
  (ivy-mode 1)
  (with-eval-after-load 'projectile
    (setq projectile-completion-system 'ivy)))

(use-package counsel
  :pin "melpa-unstable"
  :after ivy
  :bind (("C-x C-f" . counsel-find-file)
         ("C-x b"   . counsel-switch-buffer)
         ("C-c C-u" . counsel-unicode-char)
         ("C-h f"   . counsel-describe-function)
         ("C-h v"   . counsel-describe-variable)
         ("M-s f"   . counsel-file-jump)
         ("M-x"     . counsel-M-x)
         ("M-y"     . counsel-yank-pop)
         :map minibuffer-local-map
         ("C-r"     . counsel-minibuffer-history)))

(use-package ivy-rich
  :pin "melpa-unstable"
  :demand t
  :after ivy
  :custom
  (ivy-rich-path-style 'abbrev)
  :config
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line)
  (setq ivy-virtual-abbreviate 'name
        ivy-rich-path-style 'abbrev)
  (ivy-rich-project-root-cache-mode)
  (ivy-rich-mode 1))

(use-package counsel-projectile
  :pin "melpa-unstable"
  :demand t
  :after (counsel projectile)
  :diminish
  :config
  (projectile-load-known-projects)
  (counsel-projectile-mode 1))

(use-package swiper
  :pin "melpa-unstable"
  :after ivy
  :bind (:map swiper-map
              ("M-%" . swiper-query-replace))
  :bind (:map isearch-mode-map
              ("C-o" . swiper-from-isearch)))

(provide 'ix-ivy)
