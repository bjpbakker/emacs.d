;;; -*- lexical-binding: t -*-
;;; ix-os.el --- OS-specific configuration.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package os-release
  :load-path "lisp"
  :demand t
  :when (eql system-type 'gnu/linux))

(use-package nix-shell
  :load-path "lisp"
  :after os-release
  :functions nix-with-exec-path nix-with-process-environment
  :demand t
  :when (and
         (not (executable-find "direnv"))
         (and (eq system-type 'gnu/linux)
              (string= (os-release-id) "nixos")))
  :config
  (advice-add 'executable-find :around #'nix-with-exec-path)
  (advice-add 'start-process :around #'nix-with-process-environment))

(when (eq system-type 'darwin)
  (use-package exec-path-from-shell
    :config
    (setq exec-path-from-shell-variables '("PATH" "MANPATH" "NIX_PATH"))
    (exec-path-from-shell-initialize))

  (setq ns-option-modifier 'super
        ns-command-modifier 'meta))

(provide 'ix-os)
