;;; -*- lexical-binding: t -*-
;;; ix-java.el --- Make java slightly less annoying.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package cc-mode
  :ensure nil
  :preface
  (defun java-indent-config-hook ()
    (setq c-basic-offset 4
          tab-width 4)
    (c-set-offset 'arglist-intro '+)
    (c-set-offset 'arglist-close nil))
  :config
  (add-hook 'java-mode-hook #'java-indent-config-hook)
  (add-hook 'java-mode-hook #'ix/lsp-join-session))

(use-package lsp-java
  :custom
  (dap-java-test-runner (emacs-fluid-path "lsp/java/test-runner"))
  (lsp-java-workspace-dir (emacs-fluid-path "lsp/java/workspace"))
  (lsp-java-workspace-cache-dir (emacs-fluid-path "lsp/java/workspace/.cache")))

(provide 'ix-java)
