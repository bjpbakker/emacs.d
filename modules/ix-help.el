;;; -*- lexical-binding: t -*-
;;; ix-help.el --- Better help and discovery.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'package)
(require 'use-package)

(use-package which-key
  :diminish
  :commands (which-key-mode)
  :hook (emacs-startup . which-key-mode)
  :config
  (setq which-key-echo-keystrokes 0.1
        which-key-idle-delay 1.0
        which-key-idle-secondary-delay 0))

(use-package helpful
  :demand t
  :bind (("C-h k"   . helpful-key)
         ("C-h x"   . helpful-command)
         ("C-h F"   . helpful-function)
         ("C-h C-d" . helpful-at-point))
  :config
  (with-eval-after-load 'counsel
    (setq counsel-describe-function-function #'helpful-callable
          counsel-describe-variable-function #'helpful-variable)))

(provide 'ix-help)
