;;; -*- lexical-binding: t -*-
;;; ix-complete.el --- Configure completions.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package company
  :demand t
  :diminish
  :commands (company-mode)
  :bind (:map company-mode-map
              ("M-/" . company-complete-common))
  :bind (:map company-active-map
              ("C-p" . company-select-previous-or-abort)
              ("C-n" . company-select-next-or-abort))
  :hook
  (prog-mode . company-mode)
  :config
  (setq company-minimum-prefix-length 2
        company-selection-wrap-around t
        company-show-quick-access t
        company-tooltip-align-annotations t
        company-idle-delay nil
        company-require-match nil
        company-dabbrev-downcase nil
        company-dabbrev-ignore-case nil
        company-transformers '(company-sort-by-occurrence))

  (defun ix/company-number ()
    "Enhanced number input handling.

Forward to company-complete-number unless the number is potentially part of
the candidate, in which case insert the number.

See https://oremacs.com/2017/12/27/company-numbers"
    (interactive)
    (let* ((k (this-command-keys))
           (re (concat "^" company-prefix k)))
      (if (cl-find-if (lambda (s) (string-match re s))
                      company-candidates)
          (self-insert-command 1)
        (company-complete-tooltip-row (string-to-number k)))))

  (let ((map company-active-map))
    (mapc
     (lambda (n) (define-key map (format "%d" n) #'ix/company-number))
     (number-sequence 0 9))))

(use-package company-quickhelp
  :after company
  :bind (:map company-active-map
              ("C-c ?" . company-quickhelp-manual-begin))
  :config
  (setq company-quickhelp-use-propertized-text t))

(provide 'ix-complete)
