;;; -*- lexical-binding: t -*-
;;; ix-lsp.el --- work with LSP servers.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

(use-package lsp-mode
  :commands lsp
  :hook (lsp-mode . lsp-enable-which-key-integration)
  :custom
  (lsp-completion-enable t)
  (lsp-completion-provider :capf)
  (lsp-eldoc-enable-hover nil)
  (lsp-eldoc-render-all t)
  (lsp-enable-eldoc nil)
  (lsp-headerline-breadcrumb-enable nil)
  (lsp-highlight-symbol-at-point nil)
  (lsp-idle-delay 0.6)
  (lsp-inhibit-message t)
  (lsp-prefer-capf t)
  (lsp-prefer-flymake nil)
  (lsp-session-file (emacs-fluid-path "lsp/.lsp-session-v1"))
  (lsp-server-install-dir (emacs-fluid-path "lsp/servers"))
  :preface
  (defun ix/lsp-has-project-session-p (project-root)
    (if (functionp 'lsp-session)
        (let ((projects (lsp-session-folders (lsp-session))))
          (member (directory-file-name project-root) projects))
      nil))
  (defun ix/lsp-join-session ()
    (when (and (functionp 'projectile-project-root)
               (ix/lsp-has-project-session-p (projectile-project-root)))
      (lsp-mode 1)))
  :config
  (use-package lsp-lens
    :ensure nil)
  (use-package lsp-headerline
    :ensure nil))

(use-package lsp-ui
  :after lsp-mode
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-enable nil)
  (lsp-ui-doc-max-height 60)
  (lsp-ui-doc-text-scale-level 4)
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-enable nil)
  (lsp-ui-sideline-show-diagnostics nil)
  (lsp-ui-sideline-show-hover t)
  :config
  (define-key lsp-ui-mode-map [remap xref-find-definitions]
              #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references]
              #'lsp-ui-peek-find-references))

(provide 'ix-lsp)
