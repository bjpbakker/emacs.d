;;; -*- lexical-binding: t -*-
;;; ix-editor.el --- Base editor configuration.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'use-package)

;; Lines should still be bound to 80 characters.
(setq-default fill-column 80)

;; Never _ever_ indent with tabs.
(set-default 'indent-tabs-mode nil)

;; Highlight and cleanup redundant whitespace.
(use-package whitespace
  :diminish (global-whitespace-mode
             whitespace-mode
             whitespace-newline-mode)
  :commands (whitespace-bufffer
             whitespace-cleanup
             whitespace-mode
             whitespace-turn-off)
  :config
  (setq whitespace-line-column nil ;; use `fill-column' instead
        whitespace-action '(auto-cleanup untabify)
        whitespace-style '(face trailing tabs lines empty indentation tab-mark))
  :init
  (add-hook 'find-file-hook #'whitespace-mode))

(set-default 'indicate-empty-lines t)

;; Make selection work as you'd expect.
(delete-selection-mode -1)
(transient-mark-mode 1)

(use-package selected
  :pin "melpa-unstable"
  :demand t
  :hook ((prog-mode text-mode) . selected-minor-mode)
  :diminish selected-minor-mode
  :bind (:map selected-keymap
              ("=" . fill-region)
              ("d" . downcase-region)
              ("r" . reverse-region)
              ("s" . sort-lines)
              ("u" . upcase-region)
              ("?" . ix/selected-keymap-help))
  :config
  (defun ix/selected-keymap-help ()
    (interactive)
    (which-key-show-full-keymap 'selected-keymap)))

(use-package ialign
  :bind (:map selected-keymap
              ("[" . ialign)))

(use-package expand-region
  :bind ("C-=" . er/expand-region))

(use-package change-inner
  :pin "melpa-unstable"
  :bind (("M-i" . change-inner)
         ("M-o M-o" . change-outer)))

(use-package cycle-quotes
  :pin "elpa"
  :bind (("C-'" . cycle-quotes)))

(use-package undo-propose
  :commands undo-propose)

(use-package multiple-cursors
  :pin "melpa-unstable"
  :bind (("C-c m a" . mc/mark-all-dwim)
         ("C-c m W" . mc/mark-all-words-like-this)
         ("C-c m Y" . mc/mark-all-symbols-like-this)
         ("C-c m t" . mc/mark-sgml-tag-pair))
  :bind (:map selected-keymap
              ("," . mc/mark-previous-like-this)
              (">" . mc/unmark-previous-like-this)
              ("C-<" . mc/skip-to-previous-like-this)
              ("." . mc/mark-next-like-this)
              ("<" . mc/unmark-next-like-this)
              ("C->" . mc/skip-to-next-like-this)
              ("^" . mc/edit-beginnings-of-lines)
              ("$" . mc/edit-ends-of-lines)
              ("%" . mc/mark-all-in-region)
              ("a" . mc/mark-all-like-this-dwim)
              ("e" . mc/edit-lines)
              ("W" . mc/mark-previous-word-like-this)
              ("w" . mc/mark-next-word-like-this)
              ("Y" . mc/mark-previous-symbol-like-this)
              ("y" . mc/mark-next-symbol-like-this)
              ("C-c m l" . mc/insert-letters)
              ("C-c m n" . mc/insert-numbers)
              ("C-c m s" . mc/sort-regions)
              ("C-c m r" . mc/reverse-regions)))

(use-package fancy-narrow
  :bind (("C-c n n" . fancy-narrow-to-region)
         ("C-c n w" . fancy-widen))
  :commands (fancy-narrow-to-region fancy-widen))

(use-package focus
  :commands focus-mode)

(use-package centered-cursor-mode
  :pin "melpa-unstable"
  :commands (centered-cursor-mode))

(use-package volatile-highlights
  :diminish
  :config
  (volatile-highlights-mode t))

(use-package hi-lock
  :config
  (defun ix/unhighlight-all ()
    (interactive)
    (unhighlight-regexp t))
  (defun ix/unhighlight-regex-at-point ()
    (interactive)
    (let ((symb (symbol-at-point)))
      (if symb
          (mapcar (lambda (patt)
                    (when (string-match (car patt) (symbol-name symb))
                      (unhighlight-regexp (car patt))))
                  hi-lock-interactive-patterns)
        (error "No symbol at point"))))
  :bind (("M-s h >" . ix/unhighlight-regex-at-point)
         ("M-s h U" . ix/unhighlight-all)))

(use-package paren
  :config
  (show-paren-mode 1))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :pin "elpa"
  :commands rainbow-mode)

(bind-key "C-x M-i" #'insert-buffer)

(use-package editorconfig
  :hook (prog-mode . editorconfig-mode)
  :diminish editorconfig-mode)

(use-package repeat
  :custom
  (repeat-mode t))

(use-package tramp
  :ensure nil
  :bind ("C-c C-v" . ix/sudoedit-find-alternate-file)
  :config
  (defun ix/tramp-file-name-as-hop (tramp)
    (let ((tramp-file-name
           (tramp-make-tramp-file-name (tramp-file-name-method tramp)
                                       (tramp-file-name-user tramp)
                                       (tramp-file-name-domain tramp)
                                       (tramp-file-name-host tramp)
                                       (tramp-file-name-port tramp)
                                       nil
                                       (tramp-file-name-hop tramp))))
      (concat (substring tramp-file-name 1 -1)
              tramp-postfix-hop-format)))

  (defun ix/sudoedit-find-alternate-file ()
    (interactive)
    (let* ((name
            (or buffer-file-name default-directory))
           (tramp
            (and (tramp-tramp-file-p name)
                 (tramp-dissect-file-name name)))
           (host (and tramp (tramp-file-name-host tramp)))
           (localname (if tramp
                          (tramp-file-name-localname tramp)
                        name))
           (hop (and tramp (ix/tramp-file-name-as-hop tramp)))
           (sudoedit (if (executable-find "doas") "doas" "sudo")))
      (find-alternate-file
       (tramp-make-tramp-file-name sudoedit nil nil host nil localname hop)))))

(use-package vdiff
  :commands (vdiff-files
             vdiff-files3
             vdiff-buffers
             vdiff-buffers3
             vdiff-current-file
             vdiff-merge-conflict))

(provide 'ix-editor)
