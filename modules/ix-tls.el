;;; -*- lexical-binding: t -*-
;;; ix-tls.el --- Use secure TLS handshakes.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(unless (gnutls-available-p)
  (require 'tls)
  (setq tls-program
        '("gnutls-cli -p %p --dh-bits=2048 --ocsp --x509cafile=%t --priority 'SECURE128:-VERS-TLS1.1:-VERS-TLS1.0:-VERS-DTLS1.2:-VERS-DTLS1.0:-AES-128-GCM:-AES-128-CBC:-AES-128-CCM:-SHA1:-GROUP-X25519:-GROUP-X448:-GROUP-FFDHE2048:-GROUP-FFDHE3072:-GROUP-FFDHE4096:-GROUP-FFDHE6144:-SIGN-RSA-SHA256:-SIGN-RSA-PSS-SHA256:-SIGN-RSA-PSS-RSAE-SHA256:-SIGN-EdDSA-Ed25519:-SIGN-EdDSA-Ed448' %h"))
  (warn "Emacs is not using libgnutls.

TLS handshakes are configured for package `tls' instead.

Because package `tls' is obsolete since Emacs 27.1 you are adviced to enable
gnutls when you compile Emacs. See the Emacs GnuTLS chapter in the GNU Emacs
Manual [0] for more information.


[0] - https://www.gnu.org/software/emacs/manual/emacs-gnutls.html
"))

(setq gnutls-verify-error t
      gnutls-min-prime-bits 4096
      gnutls-algorithm-priority "SECURE128:-VERS-TLS1.1:-VERS-TLS1.0:-VERS-DTLS1.2:-VERS-DTLS1.0:-AES-128-GCM:-AES-128-CBC:-AES-128-CCM:-SHA1:-GROUP-X25519:-GROUP-X448:-GROUP-FFDHE2048:-GROUP-FFDHE3072:-GROUP-FFDHE4096:-GROUP-FFDHE6144:-SIGN-RSA-SHA256:-SIGN-RSA-PSS-SHA256:-SIGN-RSA-PSS-RSAE-SHA256:-SIGN-EdDSA-Ed25519:-SIGN-EdDSA-Ed448")

(provide 'ix-tls)
