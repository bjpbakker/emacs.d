;;; -*- lexical-binding: t -*-
;;; ix-direnv.el --- Support direnv environments.

;; Copyright (C) 2018-2020 Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(use-package envrc
  :demand t
  :when (executable-find "direnv")
  :diminish
  :mode ("^\\.envrc\\'" . envrc-file-mode)
  :config
  (envrc-global-mode))

(provide 'ix-direnv)
