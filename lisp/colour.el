;;; -*- lexical-binding: t -*-
;;; colour.el --- Color utilities that complement the builtin color.el.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'color)

(defun hex-byte-to-float (hex off)
  (/ (float (string-to-number
             (substring hex off (+ off 2))
             16))
     255))

(defun encode-rgb-as-triplet (rgb)
  "Convert RGB tuple to RGB integer triplets."
  (mapcar (lambda (f)
            (floor (+ (* f 255) .1)))
          rgb))

(defun encode-rgb-as-hex (rgb)
  "Encode RGB color as hex."
  (apply (lambda (r g b)
           (format "#%.2x%.2x%.2x" r g b))
         (encode-rgb-as-triplet rgb)))

(defun hex-to-hsl (hex)
  "Convert hex encoded RGB (#RRGGBB) to HSL."
  (let ((r (hex-byte-to-float hex 1))
        (g (hex-byte-to-float  hex 3))
        (b (hex-byte-to-float hex 5)))
    (color-rgb-to-hsl r g b)))

(defun hsl-to-hex (h s l)
  "Convert HSL to hex encoded RGB."
  (encode-rgb-as-hex (color-hsl-to-rgb h s l)))

(defun hex-to-rgb (hex)
  "Convert hex encoded RGB (#RRGGBB) to an RGB tuple."
  (apply #'color-hsl-to-rgb
         (hex-to-hsl hex)))

(defun rgb-to-hex (rgb)
  "Convert RGB tuple to hex encoded RGB."
  (encode-rgb-as-hex rgb))

(provide 'colour)
