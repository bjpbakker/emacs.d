;;; -*- lexical-binding: t -*-
;;; emacs-paths.el --- Support for configurable Emacs paths.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defgroup emacs-paths nil
  "Customization option for IX Emacs paths"
  :group 'emacs)

(defcustom emacs-fluid-directory
  user-emacs-directory
  "Directory beneath which Emacs-specific fluid files are placed."
  :group 'emacs-paths
  :type 'directory)

(defcustom ix-emacs-directory
  user-emacs-directory
  "Directory in which IX Emacs configuration is kept."
  :group 'emacs-paths
  :type 'directory)

(defun emacs-fluid-path (path)
  (expand-file-name path emacs-fluid-directory))

(defun ix-emacs-path (path)
  (expand-file-name path ix-emacs-directory))

(provide 'emacs-paths)
