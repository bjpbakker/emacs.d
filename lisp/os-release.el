;;; -*- lexical-binding: t -*-
;;; os-release.el --- Operating system identification.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Read options from the standard `os-release' file on GNU/Linux systems, as
;; specified in the standard [0].
;;
;; When the system does not support `os-release', this package will return `nil'
;; for all options.
;;
;; [0] - https://www.freedesktop.org/software/systemd/man/os-release.html

;;; Code:

;;;###autoload
(defun os-release-id ()
  (os-release-get "ID"))

;;;###autoload
(defun os-release-name ()
  (os-release-get "NAME"))

;;;###autoload
(defun os-release-version ()
  (os-release-get "VERSION"))

;;;###autoload
(defun os-release-version-codename ()
  (os-release-get "VERSION_CODENAME"))

;;;###autoload
(defun os-release-version-id ()
  (os-release-get "VERSION_ID"))

;;;###autoload
(defun os-release-pretty-name ()
  (os-release-get "PRETTY_NAME"))

;;;###autoload
(defun os-release-home-url ()
  (os-release-get "HOME_URL"))

;;;###autoload
(defun os-release-support-url ()
  (os-release-get "SUPPORT_URL"))

;;;###autoload
(defun os-release-get (param)
  (let ((env-alist (os-release--read)))
    (cdr (assoc param env-alist))))

(defun os-release--read ()
  (let ((file (os-release--file)))
    (when file
      (let ((lines
             (with-temp-buffer
               (insert-file-contents file)
               (split-string (buffer-string) "\n" t))))
        (mapcar 'os-release--parse-env lines)))))

(defun os-release--parse-env (line)
  (let* ((parts (split-string line "=" t))
         (raw-value (mapconcat 'identity (cdr parts) "="))
         (value
          (if (and (char-equal (string-to-char raw-value) ?\")
                   (char-equal (string-to-char (substring raw-value -1)) ?\"))
              (substring raw-value 1 -1)
            raw-value)))
    `(,(car parts) . ,value)))

(defun os-release--file ()
  (let ((file-if-exists
         (lambda (file)
           (and (file-exists-p file)
                file))))
    (or (funcall file-if-exists "/etc/os-release")
        (funcall file-if-exists "/usr/lib/os-release"))))

(provide 'os-release)
