;;; -*- lexical-binding: t -*-
;;; nix-shell.el --- Support nix-shell sandbox environments.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(defvar nix-shell--env-cache
  (make-hash-table :test 'equal)
  "Cache of (sandbox . env-alist)")

(defvar nix-shell--warn-cache
  (make-hash-table :test 'equal))

(defun nix-with-process-environment (fn &rest args)
  "Apply a function with the `process-environment' from the current sandbox.

This can be used as `:around' advice for functions that use the
`process-environment', like `make-process'."
  (let ((process-environment
         (nconc
          (list (format "PATH=%s" (mapconcat 'identity (nix-exec-path) ":")))
          process-environment)))
    (apply fn args)))

(defun nix-with-exec-path (fn &rest args)
  "Apply a function with the `exec-path' from the current sandbox.

This can be used as `:around' advice for `executable-find' or other functions
that use the `exec-path'."
  (let ((exec-path (nix-exec-path)))
    (apply fn args)))

(defun nix-executable-find (executable &optional sandbox)
  "Find an executable in the current sandbox.

Equivalent to `executable-find' with the `exec-path' from the current
sandbox."
  (locate-file executable (nix-exec-path sandbox) exec-suffixes 1))

(defun nix-shell-command (command &rest args)
  "Compose command to run in the current sandbox.

Compose a command list to run given command with arguments in the current
sandbox. `nix-shell--command-to-string' or an equivalent function can be applied
on result."
  (let ((cmd (concat command " " (mapconcat 'identity args " ")))
        (sandbox (nix-shell-find-sandbox default-directory)))
    (if sandbox
        (list "nix-shell" "--readonly-mode" "--run" cmd sandbox)
      (cons command args))))

(defun nix-exec-path (&optional sandbox)
  (let ((sandbox (or sandbox
                     (nix-shell-find-sandbox default-directory))))
    (or (and sandbox
             (nix-shell-exec-path sandbox))
        exec-path)))

(defun nix-shell-exec-path (sandbox)
  (let ((path
         (cdr (assoc "PATH" (nix-shell-env sandbox)))))
    (and path (split-string path ":" t))))

(defun nix-shell-env (sandbox)
  (let ((cached-env (gethash sandbox nix-shell--env-cache)))
    (or cached-env
        (progn
          (message "Loading Nix sandbox environment for %s..." sandbox)
          (puthash sandbox
                   (nix-shell--read-env sandbox)
                   nix-shell--env-cache)))))

(defun nix-shell-find-sandbox (path)
  (and (file-exists-p path)
       (let ((fqn
              (lambda (dir)
                (concat (expand-file-name dir)
                        "shell.nix")))
             (nix-dir (locate-dominating-file path "shell.nix")))
         (if nix-dir
             (funcall fqn nix-dir)))))

(defun nix-shell-clear-cache ()
  (interactive)
  (progn
    (clrhash nix-shell--env-cache)
    (clrhash nix-shell--warn-cache)))

(defun nix-shell--read-env (sandbox)
  (let ((env
         (nix-shell--command-to-string
           "nix-shell" "--readonly-mode" "--run" "printenv" sandbox))
        (parse-env
         (lambda (line)
           (let ((parts (split-string line "=" t)))
             `(,(car parts) . ,(mapconcat 'identity (cdr parts) "="))))))
    (if (null env)
        (nix-shell--show-init-error sandbox)
      (mapcar parse-env (split-string env "\n" t)))))

(defun nix-shell--command-to-string (command &rest args)
  (declare (indent 0))
  (let* ((tmp-buffer-name (generate-new-buffer-name "*nix-shell stdout*"))
         (stdout (generate-new-buffer tmp-buffer-name)))
    (unwind-protect
        (with-current-buffer stdout
          (let ((exit-code
                 (apply 'call-process command nil stdout nil args)))
            (when (zerop exit-code)
              (buffer-string))))
      (and (buffer-name stdout)
           (kill-buffer stdout)))))

(defun nix-shell--show-init-error (sandbox)
  (let ((time (gethash sandbox nix-shell--warn-cache))
        (now (current-time)))
    (when (or (null time)
              (> (float-time
                  (time-subtract now time)) 10))
      (puthash sandbox now nix-shell--warn-cache)
      (warn "Could not initialize nix-shell environment.

Most likely your sandbox in not fully installed. Please
run `nix-shell %s'
manually to install missing dependencies.
"
            sandbox)
      nil)))

(provide 'nix-shell)
