{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  ix-packages = (import ./nix/emacs-packages.nix);
  emacs' = emacs30.pkgs.withPackages ix-packages;

in mkShell {
  buildInputs = [ emacs' ];
}
