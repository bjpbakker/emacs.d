{
  pkgs ? import <nixpkgs> {}
, fluidDirectory ? "./tmp"
}:

with pkgs;

let
  ix-packages = import ./nix/emacs-packages.nix;

in
stdenv.mkDerivation {
  name = "ix-emacs";
  version = "dev";

  src = nix-gitignore.gitignoreSource [] ./.;

  nativeBuildInputs = [ (emacs30.pkgs.withPackages ix-packages) ];

  patchPhase = ''
    substituteInPlace init.el \
        --replace '(getenv "EMACS_FLUID_DIRECTORY")' '"./.cache"'
  '';

  installPhase = ''
    prefix=$out make install
  '';

  meta = {
    description = "ix-emacs configuration of .emacs.d";
    license = lib.licenses.gpl3;
    platforms = lib.platforms.linux ++ lib.platforms.darwin;
  };
}
