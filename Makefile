default:
.PHONY: default

SOURCES := $(wildcard *.el lisp/*.el modules/*.el modules/lang/*.el polyfills/*.el)

dist-clean:
	rm -rf custom.el elpa/ transient/ var/
.PHONY: dist-clean

install:
	$(if $(prefix),,$(error "$$prefix" is not defined))

	for f in $(SOURCES); do \
		install -D $$f $(prefix)/$$f ; \
	done

	install COPYING $(prefix)
	if test -d elpa/; then \
		cp -rp elpa/ $(prefix); \
	fi

.PHONY: install
