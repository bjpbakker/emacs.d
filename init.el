;;; -*- lexical-binding: t -*-
;;; init.el --- The start of Emacs.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(when (version< emacs-version "26.1")
  (error "Sorry, you need GNU Emacs version 26.1 or up to use IX."))

(defconst emacs-start-time (current-time))

;; Suppress GC during startup
(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 0.6)
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold 16777216
                  gc-cons-percentage 0.1)))

;; When not loading ix-emacs from ~/.emacs.d
(when load-file-name
  (setq user-emacs-directory
        (file-name-directory (file-chase-links load-file-name))))

(setq load-path
      (append `(,(expand-file-name "lisp" user-emacs-directory))
              `(,(expand-file-name "modules" user-emacs-directory))
              `(,(expand-file-name "modules/lang" user-emacs-directory))
              `(,(expand-file-name "polyfills" user-emacs-directory))
              (delete-dups load-path)))

;; Determine the fluid-directory and use it to unlitter
(let ((fluid-directory
       (or (getenv "EMACS_FLUID_DIRECTORY")
           user-emacs-directory)))

  ;; Redirect native-comp cache to a fluid location
  (when (and (fboundp 'native-comp-available-p)
             (native-comp-available-p))
    (let ((directory (expand-file-name "eln" fluid-directory)))
      (if (fboundp 'startup-redirect-eln-cache)
          (startup-redirect-eln-cache directory)
        (setcar native-comp-eln-load-path directory))))

  ;; Setup ix-emacs and fluid locations
  (require 'emacs-paths)
  (setq ix-emacs-directory user-emacs-directory
        emacs-fluid-directory fluid-directory))

(setq custom-file (emacs-fluid-path "custom.el"))
(load custom-file t)

(require 'ix-tls)
(require 'ix-package)
(require 'ix-os)
(require 'ix-unlitter)
(require 'ix-direnv)
(require 'ix-defaults)
(require 'ix-appearance)
(require 'ix-editor)
(require 'ix-navigation)
(require 'ix-help)
(require 'ix-ivy)
(require 'ix-project)
(require 'ix-vcs)
(require 'ix-complete)
(require 'ix-flycheck)
(require 'ix-misc)
(require 'ix-prog)
(require 'ix-lsp)

(require 'ix-elisp)
(require 'ix-haskell)
(require 'ix-java)
(require 'ix-javascript)
(require 'ix-json)
(require 'ix-markdown)
(require 'ix-nix)
(require 'ix-scala)
(require 'ix-typescript)
(require 'ix-yaml)

(defun uptime ()
  (float-time
   (time-subtract (current-time) emacs-start-time)))

(message "Emacs started in %.3fs" (uptime))
