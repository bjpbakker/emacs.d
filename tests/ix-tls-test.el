;;; -*- lexical-binding: t -*-
;;; ix-tls-test.el --- Test TLS connectivity.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'cl-macs)
(require 'json)

(defun test-badssl-hosts ()
  (let ((test-url
         (lambda (test)
           (let* ((p (car test))
                  (subdomain (cadr test))
                  (port (or (caddr test) 443))
                  (url (format "https://%s.badssl.com:%d/" subdomain port))
                  (response (ignore-errors (url-retrieve-synchronously url))))
             (cl-assert (funcall p response)
                        nil
                        (format "Failed '%s'" url)))))
        (pass (lambda (x) (not (null x))))
        (fail (lambda (x) (null x)))
        (skip (lambda (x) t)))

    (progn
      (mapc test-url
            `(
              ;; Certificate
              (,fail "expired")
              (,fail "wrong.host")
              (,fail "self-signed")
              (,fail "untrusted-root")
              ;; (,fail "revoked")
              ;; (,fail "pinning-test")
              (,fail "no-common-name")
              (,fail "no-subject")
              (,fail "incomplete-chain")
              (,pass "sha256")
              (,pass "sha384")
              (,pass "sha512")
              (,pass "1000-sans")
              ;; (,pass "10000-sans")
              (,pass "ecc256")
              (,pass "ecc384")
              (,pass "rsa2048")
              (,pass "rsa4096")
              (,pass "rsa8192")
              (,pass "extended-validation")

              ;; Cipher Suite
              (,fail "cbc")
              (,fail "rc4-md4")
              (,fail "rc4")
              (,fail "3des")
              (,fail "null")
              ;; (,fail "mozilla-old")
              (,pass "mozilla-intermediate")
              (,pass "mozilla-modern")

              ;; Key Exchange
              (,fail "dh480")
              (,fail "dh512")
              (,fail "dh1024")
              (,fail "dh2048")
              (,fail "dh-small-subgroup")
              (,fail "dh-composite")
              ;; (,fail "static-rsa")

              ;; Protocol
              (,fail "tls-v1-0" 1010)
              (,fail "tls-v1-1" 1011)
              (,pass "tls-v1-2" 1012)

              ;; Certificate Transparency
              ;; (,fail "no-sct")

              (,pass "preloaded-hsts")
              (,fail "subdomain.preloaded-hsts")

              ;; Known Bad
              (,fail "superfish")
              (,fail "edellroot")
              (,fail "dsdtestprovider")
              (,fail "preact-cli")
              (,fail "webpack-dev-server")

              ;; Chrome Tests
              (,fail "captive-portal")
              (,fail "mitm-software")

              ;; Defunct
              (,fail "sha1-2016")
              (,fail "sha1-2017")
              (,fail "sha1-intermediate")
              (,fail "invalid-expected-sct")))
      'success)))

(defun test-popular-sites ()
  (let ((test-url (lambda (url)
                    (cl-assert (not (null (ignore-errors (url-retrieve-synchronously url))))
                               nil
                               (format "Failed '%s'" url)))))
    (progn
      (mapc test-url
            '("https://gnu.org/"
              "https://elpa.gnu.org/"
              "https://melpa.org/"
              "https://github.com/"
              "https://gitlab.com/"
              "https://thesoftwarecraft.com/"))
      'success)))

(defun test-howsmyssl ()
  (let ((result (with-temp-buffer
                  (url-insert-file-contents "https://www.howsmyssl.com/a/check")
                  (let ((json-false :false))
                    (json-read)))))
    (let ((unknown-cipher (cdr (assoc 'unknown_cipher_suite_supported result)))
          (beast-vuln (cdr (assoc 'beast_vuln result)))
          (ephemeral-keys (cdr (assoc 'ephemeral_keys_supported result)))
          (session-tickets (cdr (assoc 'session_ticket_supported result)))
          (insecure-ciphers (cdr (assoc 'insecure_cipher_suites result)))
          (rating (cdr (assoc 'rating result))))
      (cl-assert (eq :false unknown-cipher) unknown-cipher "Unknown ciphers provided")
      (cl-assert (eq :false beast-vuln) beast-vuln "Vulnerable to BEAST")
      (cl-assert (eq 't ephemeral-keys) ephemeral-keys "No forward secrecy")
      (cl-assert (eq 't session-tickets) session-tickets "No session tickets")
      (cl-assert (null insecure-ciphers) insecure-ciphers "Insecure ciphers can be used")
      rating)))

(test-badssl-hosts)
(test-popular-sites)
(test-howsmyssl)
