;;; -*- lexical-binding: t -*-
;;; colour-test.el --- Colour test suite.

;; Copyright (C) Bart Bakker

;; Author: Bart Bakker <bart@thesoftwarecraft.com>

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'cl-macs)
(require 'colour)

(defun test-hsl-conversion ()
  (let ((go
         (lambda (hex)
           (cl-assert (string-equal hex
                                    (apply 'hsl-to-hex
                                           (hex-to-hsl hex)))))))
    (mapc go '("#000000" "#0a0b0c" "#555555" "#ffefd5" "#ffffff"))))

(defun test-rgb-conversion ()
  (let ((go
         (lambda (hex)
           (cl-assert (string-equal hex
                                    (rgb-to-hex
                                     (hex-to-rgb hex)))))))
    (mapc go '("#000000" "#0a0b0c" "#555555" "#ffefd5" "#ffffff"))))

(test-hsl-conversion)
(test-rgb-conversion)
